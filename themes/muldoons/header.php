<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?></title><!-- Shortened -->

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon-16x16.png" sizes="16x16" />

        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		

		<?php wp_head(); ?>
		
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
		
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div id="top" class="wrapper">

			<!-- header -->
			<header class="header clear color-light_bg"	>
					
					
					<!-- logo -->
					<a class="logo_link" href="#top">
							<!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
							<img src="<?php echo get_template_directory_uri(); ?>/img/muldoons.png" alt="Logo" class="logo-img">
					</a>
					<div class="nav_area">
						
					
					<!-- /logo -->
				
						<nav class="nav nav-justified table_container collapse navbar-collapse" >
							<!-- nav -->
							<?php 
							$cat_args = array( 'exclude' => '1',);
							$categories = get_categories( $cat_args); ?>
							<!-- First we loop through the categories -->
							<ul class="nav navbar-nav"> 
								<?php foreach ( $categories as $category ) : ?>
							
								<li class="menu_item"><?php 
								$args =$category->slug;
								echo '<a href="' . home_url( $path, $scheme ) . '/#' . $args .'">'. $category->name .' </a>';
								?></li>
						
								<?php endforeach; ?>
							</ul>
							
							
						</nav>

					
					<!-- /nav -->
					</div>
					<div class="social_icons text-right pull-right">
						<a href="https://twitter.com/muldoonsown" target="_blank"><em class="fa fa-twitter"></em ></a>
						<a href="mailto:info@muldoonscoffee.com?subject=Website customer enquiry" target="_blank"><em  class="fa fa-envelope"></em ></a>
						<a href="https://www.facebook.com/muldoonscoffee" target="_blank"><em  class="fa fa-facebook"></em ></a>
						
						<button type="button" class="navbar-toggle collapsed align-right col-xs-1" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar color-dark_bg"></span>
						<span class="icon-bar color-dark_bg"></span>
						<span class="icon-bar color-dark_bg"></span>
					</button>
					</div>
					
					
					

			</header>
			<!-- /header -->
		
			<div class="buy_now">
			<span class="buy_now_text_container">
			<span class="buy_now_text"> <a href="http://69.42.58.158/~shopmuldoons/" target="_blank"> Buy Now</a></span>
			</span>
			</div>
			
		
			