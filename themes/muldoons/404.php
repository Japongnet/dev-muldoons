<?php get_header(); ?>

	<div id="video" class="post_area bg_cover parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/img/panel_home_03_small.jpg"> 
		<div class="scan_lines" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/scan_lines.png');">
			
			
			<div class="video_title four_oh_four">

						<h1>Oops, looks like the page you're looking for doesn't exist. </h1>
						
						<h3>Go Back to <a href="<?php echo esc_url( home_url( '/' ) ); ?>">Muldoon's Coffee. </a></h3>
						
						<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content color-dark_bg">
				  <div>
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="color-off_black modal-title" id="myModalLabel"><em>Receive 5 Days of Free Fresh Roasted Coffee Service</em></h4>
				  </div>
				  <div class="modal-body">
				   <p class="modal_text">At Muldoon's Hand Roasted Coffee, we pride ourselves on three things; 
			quality customer service, state-of-the-art brewing systems and truly superior coffee.</p> 
			<div class="col-xs-12">
			<?php echo do_shortcode( '[contact-form-7 id="336" title="Free Trial"]' ); ?>
			</div>

					


				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				  </div>
				</div>
			  </div>
			</div>
						
			</div>
			
		</div>

	</div>

<?php get_footer(); ?>
