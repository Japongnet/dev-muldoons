<!-- Outer Loop -->
<?php 
				global $post;
				$post_slug=$post->post_name;
				
				$args = 'category_name=' . $post_slug;
				
		?>

	<div id="<?php echo $post_slug; ?>" class="post_area bg_cover post_area parallax-window" data-parallax="scroll" data-image-src="<?php the_field('page_full_background'); ?>">
	
		
		
		<?php
			
		// The Query
		$the_query = new WP_Query($args);
		
		// The Loop
		if ( $the_query->have_posts() ) { ?>
			<span class="waypoint_upper">
			</span>
			
				
				<?php
				while ( $the_query->have_posts() ) {
					$the_query->the_post(); ?>
						
						<?php // And then we make this a slide 
						get_template_part('loop'); ?>
				<?php } ?>
			
			<span class="waypoint_lower">	
			</span>	
		<?php } else {
					// no posts found
		}
		/* Restore original Post Data */
		wp_reset_postdata();
						
		?>
		
			
		
	</div>
<!-- /Outer Loop -->