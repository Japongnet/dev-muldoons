		
			
			<!-- footer -->
			<footer class="footer color-accent_bg col-xs-12 block-section text-left">

				<!-- copyright -->
				<div class="social_icons social_footer text-left">
				<span>
					<a href="https://twitter.com/muldoonsown" target="_blank"><em class="fa fa-twitter"></em ></a>
						<a href="mailto:info@muldoonscoffee.com?subject=Website customer enquiry" target="_blank"><em  class="fa fa-envelope"></em ></a>
						<a href="https://www.facebook.com/muldoonscoffee" target="_blank"><em  class="fa fa-facebook"></em ></a>
				
					
				</span>
				<!-- /copyright -->
					&copy; <?php echo date('Y'); ?> Muldoon's Coffee. Designed by <a href="http://thinkpragmatic.com/" target="_blank">Pragmatic</a>. 
					
				</div>
			</footer>
			<!-- /footer -->
			
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>
	
		
		
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.onepage-scroll.min.js"></script> 
		
		<?php 
		$cat_args = array( 'exclude' => '1',);
		$categories = get_categories( $cat_args); ?>
		<!-- First we loop through the categories -->
	
		<?php foreach ( $categories as $category ) : 
		$category_slug =$category->slug; ?>
			
			<script>	
			var waypoints = jQuery('#<?php echo $category_slug ?> .waypoint_upper').waypoint({
			  handler: function() {
				jQuery( "#<?php echo $category_slug ?> .category_trigger" ).toggleClass( "trigger_active" )
			  }
			});
			
			var waypoints = jQuery('#<?php echo $category_slug ?> .waypoint_lower').waypoint({
			  handler: function() {
				jQuery( "#<?php echo $category_slug ?> .category_trigger" ).toggleClass( "trigger_active" )
			  }
			});
			</script>
			
		<?php endforeach; ?>
		
		
		

		
		
		<script>
		jQuery(window).resize(function(){
            var parallaxHeight = Math.max(jQuery(window).height() * 0.7, 200) | 0;
            jQuery('.parallax-container').height(parallaxHeight);
		}).trigger('resize');
		</script>
		

	</body>
</html>
