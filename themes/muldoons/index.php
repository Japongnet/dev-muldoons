<?php get_header(); ?>
	<!-- Index -->
	<!-- Loop through the pages  -->
	
	<div id="video" class="post_area bg_cover parallax-window" data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/img/panel_home_03_small.jpg"> 
		<div class="scan_lines" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/scan_lines.png');">
			
			
			<div class="video_title">

						<a href="#" class="title_image"><div class="title_image" style="background-image:url('<?php echo get_template_directory_uri(); ?>/img/title_image.png');" data-toggle="modal" data-target="#myModal" ></div></a>
						
						<!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content color-dark_bg">
				  <div>
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="color-off_black modal-title" id="myModalLabel"><em>Receive 5 Days of Free Fresh Roasted Coffee Service</em></h4>
				  </div>
				  <div class="modal-body">
				   <p class="modal_text">At Muldoon's Hand Roasted Coffee, we pride ourselves on three things; 
			quality customer service, state-of-the-art brewing systems and truly superior coffee.</p> 
			<div class="col-xs-12">
			<?php echo do_shortcode( '[contact-form-7 id="336" title="Free Trial"]' ); ?>
			</div>

					


				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				  </div>
				  </div>
				</div>
			  </div>
			</div>
						
			</div>
			
		</div>
		<!--
		<video autoplay loop muted id="bgvid">
			<source src="<?php echo get_template_directory_uri(); ?>/img/Comp_1_1_2.mp4" type="video/webm">
			
		</video>
		-->
	</div>
	<?php
	$wp_query_args = array(

	'post_type'   => 'page',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'order'   => 'ASC',
	);
	// The Query
	$the_query = new WP_Query($wp_query_args);
	// The Loop
	if ( $the_query->have_posts() ) { ?>
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
			<?php get_template_part('outer_loop');?>
		<?php endwhile; ?>	
	<?php }

	 else {
		 echo 'No Posts Found';
	}
	/* Restore original Post Data */
	wp_reset_postdata();

	?>
			
				
	<!-- /Index -->
<?php get_footer(); ?>
