<!-- Loop -->
	
	<!-- article -->
	
	<article id="post-<?php the_ID(); ?>">
		<div class="post_area bg_cover" style="background-image:url('<?php the_field('post_full_background'); ?>')">

			
			<div class="post_area_1 <?php the_field('post_area_1_class'); ?>" style="background-image:url('<?php the_field('post_area_1_background'); ?>')">
				<div class="col-xs-12">
				<?php the_field('post_area_1'); ?>
				
				</div>
				<div class="trail">
				</div>
			</div>
			
			
			
	

				<div class="post_area_2 <?php the_field('post_area_2_class'); ?>" style="background-image:url('<?php the_field('post_area_2_background'); ?>')">
					<div class="col-xs-12">
					<?php the_field('post_area_2'); ?>
					
					</div>
					<div class="trail">
				</div>
				</div>
				
			
			
			
			
		</div>
	</article>
	
	<!-- /article -->

<!-- /loop -->
